import React, { Component } from "react";
import {
  View,
  StatusBar,
  SafeAreaView,
  KeyboardAvoidingView
} from "react-native";
import { Container } from "../component/container";
import { Logo } from "../component/logo";
import { InputTextButton } from "../component/TextInput";
import { ClearButton } from "../component/Button";
import { LastConverted } from "../component/Text";
import { Header } from "../component/header";
import CurrencyList from "./CurrencyList";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { connectAlert } from "../component/Alert";
import {
  changeCurrencyAmount,
  swapCurrency,
  getInitialConversion
} from "../actions/currencies";

const TEMP_BASE_CURRENCY = "USD";
const TEMP_QUOTE_CURRENCY = "GBP";
const TEMP_BASE_PRICE = "100";
const TEMP_QUOTE_PRICE = "79.74";
const TEMP_CONVERSION_DATE = new Date();
const TEMP_CONVERSION_RATE = "0.7974";

class Home extends Component {
  static propTypes = {
    navigation: PropTypes.object,
    dispatch: PropTypes.func,
    baseCurrency: PropTypes.string,
    quoteCurrency: PropTypes.string,
    amount: PropTypes.number,
    conversionRate: PropTypes.number,
    lastConvertedDate: PropTypes.object,
    isFetching: PropTypes.bool,
    primaryColor: PropTypes.string,
    currencyError: PropTypes.string,
    alertWithType: PropTypes.func
  };
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(getInitialConversion());
  }
  componentWillReceiveProps(nextProps) {
    const { currencyError, alertWithType } = this.props;
    if (nextProps.currencyError && !currencyError) {
      alertWithType("error", "Error", nextProps.currencyError);
    }
  }

  handlePressBaseCurrency = () => {
    const { navigation } = this.props;
    navigation.navigate("CurrencyList", {
      title: "Base Currency",
      type: "base"
    });
  };
  handlePressQuoteCurrency = () => {
    const { navigation } = this.props;
    navigation.navigate("CurrencyList", {
      title: "Quote Currency",
      type: "quote"
    });
  };

  handleChangeText = text => {
    const { dispatch } = this.props;
    dispatch(changeCurrencyAmount(text));
  };
  handleSwapCurrencies = () => {
    const { dispatch } = this.props;
    dispatch(swapCurrency());
  };
  hanldeOptionPress = () => {
    const { navigation } = this.props;
    navigation.navigate("Options");
  };

  render() {
    const {
      isFetching,
      amount,
      conversionRate,
      baseCurrency,
      quoteCurrency,
      lastConvertedDate,
      primaryColor
    } = this.props;

    let quotePrice = "...";
    if (!isFetching) {
      quotePrice = (amount * conversionRate).toFixed(2);
    }

    return (
      <Container backgroundColor={primaryColor}>
        <StatusBar translucent={false} barStyle={"light-content"} />
        <Header onPress={this.hanldeOptionPress} />
        <KeyboardAvoidingView
          behavior="padding"
          style={{ alignItems: "center" }}
        >
          <Logo tintColor={primaryColor} />
          <InputTextButton
            onPress={this.handlePressBaseCurrency}
            buttonText={baseCurrency}
            defaultValue={amount.toString()}
            keyboardType="numeric"
            onChangeText={this.handleChangeText}
            textColor={primaryColor}
          />
          <InputTextButton
            onPress={this.handlePressQuoteCurrency}
            buttonText={quoteCurrency}
            editable={false}
            value={quotePrice}
            textColor={primaryColor}
          />
          <LastConverted
            date={lastConvertedDate}
            base={baseCurrency}
            quote={quoteCurrency}
            conversionRate={conversionRate}
          />
          <ClearButton
            text="Reverse Currencies"
            onPress={this.handleSwapCurrencies}
          />
        </KeyboardAvoidingView>
      </Container>
    );
  }
}
const mapStateToProps = state => {
  const { baseCurrency, quoteCurrency } = state.currencies;
  const conversionSelector = state.currencies.conversions[baseCurrency] || {};
  const rates = conversionSelector.rates || {};

  return {
    baseCurrency,
    quoteCurrency,
    amount: state.currencies.amount,
    conversionRate: rates[quoteCurrency] || 0,
    lastConvertedDate: conversionSelector.date
      ? new Date(conversionSelector.date)
      : new Date(),
    isFetching: conversionSelector.isFetching,
    primaryColor: state.theme.primaryColor,
    currencyError: state.currencies.error
  };
};

export default connect(mapStateToProps)(connectAlert(Home));
