import React from "react";
import { Text } from "react-native";
import moment from "moment";
import PropTypes from "prop-types";
import style from "./style";

const LastConverted = ({ base, date, quote, conversionRate }) => (
  <Text style={style.smallText}>
    1 {base} = {conversionRate.toString()} {quote} as of{" "}
    {moment(date).format("MMMM D, YYYY")}
  </Text>
);

LastConverted.propTypes = {
  date: PropTypes.object,
  base: PropTypes.string,
  quote: PropTypes.string,
  conversionRate: PropTypes.number
};

export default LastConverted;
