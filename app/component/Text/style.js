import EStyleSheet from "react-native-extended-stylesheet";

const style = EStyleSheet.create({
  smallText: {
    fontSize: 12,
    color: "white",
    alignItems: "center"
  }
});

export default style;
