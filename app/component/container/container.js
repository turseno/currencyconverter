import React from "react";
import { View, TouchableWithoutFeedback, Keyboard } from "react-native";
import PropTypes from "prop-types";
import style from "./style";

const Container = ({ children, backgroundColor }) => {
  const containerStyles = [style.container];
  if (backgroundColor) {
    containerStyles.push({ backgroundColor });
  }
  return <View style={containerStyles}>{children}</View>;
};

Container.propTypes = {
  children: PropTypes.element,
  backgroundColor: PropTypes.string
};

export default Container;
