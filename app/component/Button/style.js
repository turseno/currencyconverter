import EStyleSheet from "react-native-extended-stylesheet";

const style = EStyleSheet.create({
  container: {
    alignItems: "center"
  },
  wrapper: {
    flexDirection: "row",
    alignItems: "center"
  },
  icon: {
    width: 19,
    marginRight: 11
  },
  text: {
    fontSize: 14,
    color: "white",
    fontWeight: "300",
    paddingVertical: 11
  }
});

export default style;
