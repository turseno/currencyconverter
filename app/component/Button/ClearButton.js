import React from "react";
import PropTypes from "prop-types";
import { Image, Text, View, TouchableOpacity } from "react-native";
import style from "./style";

const ClearButton = ({ text, onPress }) => (
  <TouchableOpacity style={style.container} onPress={onPress}>
    <View style={style.wrapper}>
      <Image
        resizeMode="contain"
        style={style.icon}
        source={require("./images/icon.png")}
      />
      <Text style={style.text}>{text}</Text>
    </View>
  </TouchableOpacity>
);

ClearButton.propTypes = {
  text: PropTypes.string,
  onPress: PropTypes.func
};

export default ClearButton;
