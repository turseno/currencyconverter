import React from "react";
import { View, Image, TouchableOpacity } from "react-native";
import style from "./style";
import PropTypes from "prop-types";

const Header = ({ onPress }) => (
  <View style={style.container}>
    <TouchableOpacity style={style.button} onPress={onPress}>
      <Image
        resizeMode="contain"
        style={style.icon}
        source={require("./images/gear.png")}
      />
    </TouchableOpacity>
  </View>
);

Header.propTypes = {
  onPress: PropTypes.func
};
export default Header;
