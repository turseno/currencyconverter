import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ImageBackground,
  Keyboard,
  Animated,
  StyleSheet,
  Platform
} from "react-native";
import style from "./style";
import PropTypes from "prop-types";

const ANIMATION_DURATION = 250;

class Logo extends Component {
  static propTypes = {
    tintColor: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      containerImageWidth: new Animated.Value(style.$largeContainerSize),
      imageWidth: new Animated.Value(style.$largeImageSize)
    };
  }

  componentDidMount() {
    const name = Platform.OS === "ios" ? "Will" : "Did";
    this.keyboardDidShowListener = Keyboard.addListener(
      `keyboard${name}Show`,
      this.keyboardWillShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      `keyboard${name}Hide`,
      this.keyboardWillHide
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  keyboardWillShow = () => {
    const { containerImageWidth, imageWidth } = this.state;

    Animated.parallel([
      Animated.timing(containerImageWidth, {
        toValue: style.$smallContainerSize,
        duration: ANIMATION_DURATION
      }),
      Animated.timing(imageWidth, {
        toValue: style.$smallImageSize,
        duration: ANIMATION_DURATION
      })
    ]).start();
  };

  keyboardWillHide = () => {
    const { containerImageWidth, imageWidth } = this.state;

    Animated.parallel([
      Animated.timing(containerImageWidth, {
        toValue: style.$largeContainerSize,
        duration: ANIMATION_DURATION
      }),
      Animated.timing(imageWidth, {
        toValue: style.$largeImageSize,
        duration: ANIMATION_DURATION
      })
    ]).start();
  };

  render() {
    const { containerImageWidth, imageWidth } = this.state;
    const { tintColor } = this.props;

    const containerImageStyles = [
      style.containerImage,
      { width: containerImageWidth, height: containerImageWidth }
    ];
    const imageStyles = [
      style.logo,
      { width: imageWidth },
      tintColor ? { tintColor } : null
    ];

    return (
      <View style={style.container}>
        <Animated.View style={containerImageStyles}>
          <Animated.Image
            resizeMode="contain"
            style={[StyleSheet.absoluteFill, containerImageStyles]}
            source={require("./images/background.png")}
          />
          <Animated.Image
            resizeMode="contain"
            style={imageStyles}
            source={require("./images/logo.png")}
          />
        </Animated.View>
        <Text style={style.text}>Currency Converter</Text>
      </View>
    );
  }
}
export default Logo;
