import EStyleSheet from "react-native-extended-stylesheet";
import { StyleSheet } from "react-native";

const HEIGHT = 48;
const BORDER_RADIUS = 4;
const styles = EStyleSheet.create({
  $buttonBackgroundColorBase: "white",
  $buttonBackgroundColorModifier: 0.5,

  border: {
    height: HEIGHT,
    width: StyleSheet.hairlineWidth,
    backgroundColor: "red"
  },
  container: {
    backgroundColor: "white",
    width: "90%",
    height: HEIGHT,
    borderRadius: BORDER_RADIUS,
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 11
  },
  buttonContainer: {
    height: HEIGHT,
    alignItems: "center",
    justifyContent: "center",
    borderTopLeftRadius: BORDER_RADIUS,
    borderBottomLeftRadius: BORDER_RADIUS
  },
  buttonText: {
    fontWeight: "600",
    fontSize: 20,
    paddingHorizontal: 16,
    color: "$primaryBlue"
  },
  textInput: {
    height: HEIGHT,
    flex: 1,
    fontSize: 18,
    paddingHorizontal: 8,
    color: "black"
  },
  containerDisable: {
    backgroundColor: "#F0F0F0"
  }
});

export default styles;
