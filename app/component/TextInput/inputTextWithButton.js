import React from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableHighlight, TextInput } from "react-native";
import style from "./style";
import color from "color";

const InputWithButton = props => {
  const { onPress, buttonText, editable = true } = props;
  const underlayColor = color(style.$buttonBackgroundColorBase).darken(
    style.$buttonBackgroundColorModifier
  );
  const containerStyle = [style.container];

  if (editable === false) {
    containerStyle.push(style.containerDisable);
  }
  return (
    <View style={style.container}>
      <TouchableHighlight
        underlayColor={underlayColor}
        style={style.buttonContainer}
        onPress={onPress}
      >
        <Text style={style.buttonText}>{buttonText}</Text>
      </TouchableHighlight>
      <View style={style.border} />
      <TextInput
        style={style.textInput}
        underlineColorAndroid="transparent"
        {...props}
      />
    </View>
  );
};

InputWithButton.propTypes = {
  buttonText: PropTypes.string,
  onPress: PropTypes.func,
  editable: PropTypes.bool,
  textColor: PropTypes.string
};

export default InputWithButton;
