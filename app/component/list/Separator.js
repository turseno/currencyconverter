import React from "react";
import style from "./style";
import { View } from "react-native";

const Separator = () => <View style={style.separator} />;

export default Separator;
