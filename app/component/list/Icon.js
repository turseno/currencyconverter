import PropTypes from "prop-types";
import React from "react";
import { View, Image } from "react-native";

import style from "./style";

const Icon = ({ visible, checkmark, iconBackground }) => {
  if (visible) {
    const iconStyles = [style.icon];
    if (visible) {
      iconStyles.push(style.iconVisible);
    }

    if (iconBackground) {
      iconStyles.push({ backgroundColor: iconBackground });
    }

    return (
      <View style={iconStyles}>
        {checkmark ? (
          <Image
            source={require("./images/check.png")}
            style={style.checkIcon}
            resizeMode="contain"
          />
        ) : null}
      </View>
    );
  }

  return <View style={style.icon} />;
};

Icon.propTypes = {
  visible: PropTypes.bool,
  checkmark: PropTypes.bool,
  iconBackground: PropTypes.string
};

export default Icon;
