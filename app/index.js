import React from "react";
import Home from "./screens/Home";
import EStyleSheet from "react-native-extended-stylesheet";
import CurrencyList from "./screens/CurrencyList";
import Themes from "./screens/Themes";
import Navigator from "./config/routes";
import { AlertProvider } from "./component/Alert";
import { Provider } from "react-redux";
import store from "./config/store";
EStyleSheet.build({
  $primaryBlue: "#4F6D7A"
});

export default () => (
  <Provider store={store}>
    <AlertProvider>
      <Navigator onNavigationStateChange={null} />
    </AlertProvider>
  </Provider>
);
